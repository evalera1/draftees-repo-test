
############################################################################################################
# Resource Group
############################################################################################################
resource "azurerm_resource_group" "rg-env1-test-001" {
  name     = "rg-env1-test-001"
  location = "eastus"
}

############################################################################################################
# Virtual Networks
############################################################################################################
resource "azurerm_virtual_network" "vnet-env1-test-001" {
  name                = "vnet-env1-test-001"
  location            = "eastus"
  resource_group_name = "rg-env1-test-001"
  address_space       = ["10.0.0.0/16"]

  tags = {
    environment = "Test"
  }

  depends_on = [
    azurerm_resource_group.rg-env1-test-001
  ]
}

############################################################################################################
# Subnets
############################################################################################################
resource "azurerm_subnet" "snet-env1-test-001" {
  name                 = "snet-env1-test-001"
  resource_group_name  = "rg-env1-test-001"
  virtual_network_name = "vnet-env1-test-001"
  address_prefixes     = ["10.0.0.0/24"]

  depends_on = [
    azurerm_virtual_network.vnet-env1-test-001
  ]
}

############################################################################################################
# Public IP Addresses
############################################################################################################
resource "azurerm_public_ip" "pip-env1-test-001" {
  name                = "pip-env1-test-001"
  resource_group_name = "rg-env1-test-001"
  location            = "eastus"
  allocation_method   = "Static"

  tags = {
    environment = "Test"
  }

  depends_on = [
    azurerm_virtual_network.vnet-env1-test-001
  ]
}

############################################################################################################
# Network Security Groups
############################################################################################################
resource "azurerm_network_security_group" "nsg-env1-test-001" {
  name                = "nsg-env1-test-001"
  location            = "eastus"
  resource_group_name = "rg-env1-test-001"

  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3389"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "Test"
  }

  depends_on = [
    azurerm_virtual_network.vnet-env1-test-001
  ]
}

############################################################################################################
# Network Interfaces Cards
############################################################################################################
resource "azurerm_network_interface" "nic-env1-test-001" {
  name                = "nic-env1-test-001"
  location            = "eastus"
  resource_group_name = "rg-env1-test-001"

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.snet-env1-test-001.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.pip-env1-test-001.id
  }

  tags = {
    environment = "Test"
  }

  depends_on = [
    azurerm_virtual_network.vnet-env1-test-001,
    azurerm_public_ip.pip-env1-test-001,
    azurerm_subnet.snet-env1-test-001
  ]
}

resource "azurerm_network_interface" "nic-env1-test-002" {
  name                = "nic-env1-test-002"
  location            = "eastus"
  resource_group_name = "rg-env1-test-001"

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.snet-env1-test-001.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.pip-env1-test-001.id
  }

  tags = {
    environment = "Test"
  }

  depends_on = [
    azurerm_virtual_network.vnet-env1-test-001,
    azurerm_public_ip.pip-env1-test-001,
    azurerm_subnet.snet-env1-test-001
  ]
}

resource "azurerm_network_interface_security_group_association" "nsg-assoc-env1-test-001" {
  network_interface_id      = azurerm_network_interface.nic-env1-test-001.id
  network_security_group_id = azurerm_network_security_group.nsg-env1-test-001.id
}

##################################################################################
# Virtual Machine - Legacy Windows
##################################################################################

resource "azurerm_virtual_machine" "vm_legacy" {
  count = var.vm_legacy_select ? 1 : 0

  name                  = "test-vm1"
  location              = "eastus"
  resource_group_name   = "rg-env1-test-001"
  network_interface_ids = [azurerm_network_interface.nic-env1-test-001.id]
  vm_size               = "Standard_B1s"

  # Uncomment this line to delete the OS disk automatically when deleting the VM
  delete_os_disk_on_termination = true

  # Uncomment this line to delete the data disks automatically when deleting the VM
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2022-Datacenter"
    version   = "latest"
  }
  storage_os_disk {
    name              = "test-vm1-osdisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile_windows_config {
    provision_vm_agent        = true
    enable_automatic_upgrades = false
  }
  os_profile {
    computer_name  = "test-vm1"
    admin_username = "azureadmin"
    admin_password = "Password123456"
  }
  tags = {
    environment = "test"
  }
}


