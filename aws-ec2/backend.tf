#backend
terraform{
    backend "s3" {
        bucket = "drafteees123456"
        key = "state_files/ec2/terraform.tfstate"
        region = "us-west-2"
        encrypt = true
        #dynamodb_table = "terraform-state-lock"
    }
}